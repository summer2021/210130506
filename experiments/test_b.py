import numpy as np
import oneflow as flow
import oneflow.nn as nn

flow.enable_eager_execution()
flow.enable_dtr(True)

cuda0 = flow.device('cuda:0')

x1 = flow.zeros((20, 16, 50), device=cuda0).normal_()
x2 = flow.zeros((20, 16, 50), device=cuda0).normal_()

z = x1 + x2

m = nn.Conv1d(16, 33, 3, stride=2, bias=False)
m.to(cuda0)

out = m(z)
print(z.size())
