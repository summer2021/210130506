import oneflow as flow
import oneflow.nn as nn

flow.enable_eager_execution()
flow.enable_dtr(True, 0.019, False)

cuda0 = flow.device('cuda:0')

# TEST NN.LINEAR
x1 = flow.randn(1550, 1550, device=cuda0, requires_grad=True)
x2 = flow.randn(1550, 1550, device=cuda0, requires_grad=True)

m = nn.Linear(1550, 1550, bias=False)
m.to(cuda0)

z1 = m(x1)
print(z1.shape)

loss = z1 - x2
loss2 = loss * loss
loss3 = loss2.sum()
print(loss3)

# # TEST NN.CONV1D
# x1 = flow.randn(32, 36, 2048, device=cuda0, requires_grad=True)
# print(x1.shape)
# x2 = flow.randn(32, 36, 2048, device=cuda0, requires_grad=True)
# print(x2.shape)
# y1 = x1 + x2
# print(y1.shape)
# m = nn.Conv1d(36, 64, 1024, 1, 3, bias=False)
# m.to(cuda0)
# print('m.weight.grad: ', m.weight.grad)
# z1 = m(y1)
# print('+++++++++++++++z1.shape: ', z1.shape)

# y2 = y1 + x1
# print(y2.shape)
# y3 = y1 + x2
# print(y3.shape)
# y4 = y2 + y2
# print(y4.shape)
