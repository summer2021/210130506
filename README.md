本项目为 OneFlow eager 模式注入亚线性显存优化的逻辑，即在动态计算图上通过 tensor 的自动重计算，保证训练过程中显存占用始终小于某一阈值，为机器显存受限时训练大模型和使用更大的 batch size 提供可能。DTR 策略来自 ICLR 2021 文章：Dynamic Tensor Rematerialization

理想用法：为用户提供接口进入 DTR 模式。随后系统根据当前计算需求及显存的占用情况自动释放和重计算位于 memory pool 中的 tensors。

希望在尽量少改动 oneflow 原始框架的基础上完成以上任务。

TODO:

- [x] 为 DTR tensor 建立独立的 Tensor 类型、Implementation、Interpreter；
- [x] 为 DTRTensor 加入如 compute_time, size, last_access_time, compute_path 等特有属性；
- [x] 设定显存阈值，实现自动释放 DTR tensor；
- [ ] 优化 tensor neighbor cost 的计算。

submodule oneflow 的编译可参考：https://github.com/Oneflow-Inc/oneflow
